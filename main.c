#include <stdlib.h>
#include <stdio.h>
#include "funkcje.h"

int main() 
{

    FILE* f;
    f = fopen("values.txt", "r+");

    float *x = (float*)malloc(50 * sizeof(int));
    float *y = (float*)malloc(50 * sizeof(int));
    float *RHO = (float*)malloc(50 * sizeof(int));

    fscanf(f,"%*s %*s %*s %*s");   // skip first line    

    for (size_t i = 0; i < 50; i++)
    {
        fscanf(f, "%*s %f %f %f", &x[i], &y[i], &RHO[i]);
    }
    for (size_t i = 0; i < 50; i++)
    {
        printf("x = %.5f , y = %f , RHO = %.3f\n", x[i], y[i], RHO[i]); //sprawdzenie przypisania
    }

    float med_x, med_y, med_RHO;

    float avg_x, avg_y, avg_RHO; 

    float sd_x, sd_y, sd_RHO;

    med_x = mediana(x);
    med_y = mediana(y);
    med_RHO = mediana(RHO);
    
    avg_x = avarage(x);
    avg_y = avarage(y);  
    avg_RHO = avarage(RHO);

    sd_x = st_dev(x);
    sd_y = st_dev(y);
    sd_RHO = st_dev(RHO);

    printf("mediana : x = %f y = %f RHO = %f\n", med_x , med_y, med_RHO);
    printf("srednia : x = %f y = %f RHO = %f\n", avg_x , avg_y, avg_RHO);
    printf("odchylenie standardowe : x = %f y = %f RHO = %f\n", sd_x , sd_y, sd_RHO);

    fprintf(f,"mediana : x = %f y = %f RHO = %f\n", med_x , med_y, med_RHO);
    fprintf(f,"srednia : x = %f y = %f RHO = %f\n", avg_x , avg_y, avg_RHO);
    fprintf(f,"odchylenie standardowe : x = %f y = %f RHO = %f\n", sd_x , sd_y, sd_RHO);

    free(x);
    free(y);
    free(RHO);

    fclose(f);
    return 0;
}