#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mediana.h"
#include "srednia.h"
#include "odchylenie standardowe.h"

int main()
{
    int i = 0;
    int num = 0;
    float median_x = 0.0;
    float median_y = 0.0;
    float median_RHO = 0.0;
    float avg_x = 0.0;
    float avg_y = 0.0;
    float avg_RHO = 0.0;
    FILE*values;
    values = fopen("values.txt" , "r");
        float *x = (float*)malloc(50 * sizeof(float));
        float *y = (float*)malloc(50 * sizeof(float));
        float *RHO = (float*)malloc(50 * sizeof(float));
    for (size_t i = 1; i < 50; i++)
    {
        fscanf(values , "%d.\t%f\t%f\t%.3f\n",&num, &x[i], &y[i], &RHO[i]);
    }
    for (size_t i = 0; i < 49; i++)
    {
        printf("x = %f y = %f RHO = %f\n", x[i], y[i], RHO[i]);
    }

    median_x = mediana(x);
    median_y = mediana(y);
    median_RHO = mediana(RHO);
    /*avg_x = srednia(x);
    avg_y = srednia(y);
    avg_RHO = srednia(RHO);*/

    printf("mediana dla: x = %f y = %f, RHO = %f\n", median_x,median_y,median_RHO);
    printf("srednia dla: x = %f y = %f, RHO = %f\n", avg_x,avg_y,avg_RHO);


    free(x);
    free(y);
    free(RHO);
    fclose(values);
    return 0;
}